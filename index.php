<!DOCTYPE html>
<html>
    <head>

        <title>#1 Inputs Dinâmicos - Criando, Selecionando e Aplicando Máscaras</title>
        <link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-reboot.min.css">
        <link rel="stylesheet" type="text/css" href="resources/css/bootstrap-grid.min.css">
        <link rel="stylesheet" type="text/css" href="resources/css/fontawesome-all.min.css">
        <link rel="stylesheet" type="text/css" href="css/main.css">

    </head>
    <body>

        <div class="container">

            <header>

                <div class="row my-3">

                    <div class="col-12 text-center">

                        <h5>#1 Elementos Dinâmicos - Criando, Selecionando e Manipulando</h5>

                    </div>

                </div>

            </header>

            <main>

                <form id="employees_form" action="" method="post">

                    <div class="row employees_row">

                        <div class="col-12 col-md-6 col-lg-4 mb-1" data-key="1">

                            <div class="card">

                                <div class="card-header">
                                    <span class="align-middle font-bold">Funcionários</span>
                                </div>

                                <div class="card-body">

                                    <div class="employee_box row">

                                        <div class="col-12 mb-1">

                                            <label>Nome:</label>
                                            <input type="text" class="form-control name" name="name[]" />

                                        </div>

                                        <div class="col-6">

                                            <label>Nascimento:</label>
                                            <input type="text" class="form-control birthday" name="birthday[]" />

                                        </div>

                                        <div class="col-6">

                                            <label>Salário:</label>
                                            <input type="text" class="form-control salary" name="salary[]" />

                                        </div>

                                        <div class="col-12"><hr /></div>

                                    </div>

                                </div>

                                <div class="card-footer">
                                    
                                    <button type="button" class="btn btn-sm btn-primary float-right add_employee"><i class="fa fa-plus"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger float-right rem_employee mr-1 d-none"><i class="fa fa-minus"></i></button>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </main>
        </div>

        <script type="text/javascript" src="resources/js/jquery-3.3.1.min.js"></script>
        <script type="text/javascript" src="resources/js/jquery.mask.js"></script>
        <script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/main.js"></script>

    </body>
</html>